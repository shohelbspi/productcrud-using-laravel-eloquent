<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {

        $data = Category::latest()->paginate(10);

        return view('forntend.layouts.category.index', compact('data'));
    }


    public function create()
    {

        return view('forntend.layouts.category.create');
    }


    public function store(Request $request)
    {
        $data = new Category();
        $data->name = $request->category_name;
        $data->slug = $request->slug;

        $data->save();
        return redirect()->route('index.category');
    }

    public function view($id)
    {
        $view = Category::find($id);

        return view('forntend.layouts.category.view', compact('view'));

    }

    public function edit($id)
    {
        $edit = Category::find($id);

        return view('forntend.layouts.category.edit', compact('edit'));

    }

    public function update(Request $request, $id)
    {
        $data = Category::find($id);

        $data->name = $request->category_name;

        $data->save();

        return redirect()->route('index.category');

    }

    public function destory($id)
    {
        $data = Category::find($id)->delete();
       
        return redirect()->route('index.category');
    }

}
