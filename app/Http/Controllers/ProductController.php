<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        $data = Product::latest()->paginate(10);

        return view('forntend.layouts.product.index', compact('data'));
    }
    


    public function create()

    {
        $category = Category::all();

        return view('forntend.layouts.product.create', compact('category'));
    }

    
    public function store(Request $request)
    {
        $data = new Product();
        $data->name = $request->name;
        $data->title = $request->title;
        $data->price = $request->price;
        $data->description = $request->description;
        $data->cat_id = $request->category_id;

        $data->save();
        return redirect()->route('index.product');
    }
}
