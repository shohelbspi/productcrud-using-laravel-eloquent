<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('forntend.layouts.master');
});


// Category Route

Route::get('/category-create', [CategoryController::class,'create'])->name('create.category');
Route::get('/category-index', [CategoryController::class,'index'])->name('index.category');
Route::post('/category-store', [CategoryController::class,'store'])->name('store.category');
Route::get('/category-view/{id}', [CategoryController::class,'view'])->name('view.category');
Route::get('/category-edit/{id}', [CategoryController::class,'edit'])->name('edit.category');
Route::post('/category-update/{id}', [CategoryController::class,'update'])->name('update.category');
Route::get('/category-destory/{id}', [CategoryController::class,'destory'])->name('destory.category');


//product route 

Route::get('/product-create', [ProductController::class,'create'])->name('create.product');
Route::get('/product-index', [ProductController::class,'index'])->name('index.product');
Route::post('/product-store', [ProductController::class,'store'])->name('store.product');
Route::get('/product-view/{id}', [ProductController::class,'view'])->name('view.product');
Route::get('/product-edit/{id}', [ProductController::class,'edit'])->name('edit.product');
Route::post('/product-update/{id}', [ProductController::class,'update'])->name('update.product');
Route::get('/product-destory/{id}', [ProductController::class,'destory'])->name('destory.product');
