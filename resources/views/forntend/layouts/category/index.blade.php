@extends('forntend/layouts/master')

@section('title')
Categoty Index
@endsection


@section('Content')
<div class = "container my-4">
<div class = "card">
<div class = "card-header">
<a   href  = "{{ route ('create.category') }}"><button style = "float: right" class = "btn btn-success">Add
                    Category</button></a>
            <h2>All Category</h2>
        </div>
        <div   class = "card-body">
        <table class = "table">
                <thead>
                    <tr>
                        <th scope = "col">Id</th>
                        <th scope = "col">Name</th>
                        <th scope = "col " class="text-center">Actions</th>
                    </tr>
                </thead>



                <tbody>

                    @foreach ($data as $item)
                  
                    <tr> 
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td class = 'mx-2 d-flex justify-content-center'>
                        <a  href  = "{{ route ('view.category',$item->id) }}"><button
                            class = "btn btn-success mr-1">View</button></a>

                        <a  href  = "{{ route ('edit.category',$item->id) }}"><button
                            class = "btn btn-primary mr-1">Edit</button></a>

                        <a  href  = "{{ route ('destory.category',$item->id) }}" onclick="return confirm('Are you absolutely sure you want to delete?')"><button
                            class = "btn btn-danger">Delete</button></a>
                        </td>
                    </tr> 

                    @endforeach
                </tbody>
            </table>
           <div class="float-right">
            {{ $data->links('pagination::bootstrap-4') }}

           </div>
        </div>
    </div>
</div>


@endsection
