@extends('forntend/layouts/master')

@section('title')
    Categoty View
@endsection


@section('Content')
<div class="container my-4">
    <div class="card" style="width: 30%">
        <div class="card-header">
            <a href="{{route ('index.category')}}"><button style="float: right" class="btn btn-success">All Category</button></a>
            Category Data
        </div>
        <div class="card-body">
            <p>Category Name: {{$view->name}}</p>
        </div>
    </div>
</div>

@endsection
