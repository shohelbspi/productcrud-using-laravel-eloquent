@extends('forntend/layouts/master')

@section('title')
Product Create
@endsection


@section('Content')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{ route ('index.product') }}"><button  type="button" style="float: right" class="btn btn-success">All Products</button></a>
            <h2>Create Product</h2>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route ('store.product') }}">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Product Name</label>
                    <input type="text" class="form-control" id="name" name="name"
                        aria-describedby="name">
                </div>

                <div class="mb-3">
                    <label for="title" class="form-label">Product Title</label>
                    <input type="text" class="form-control" id="title" name="title"
                        aria-describedby="title">
                </div>

                <div class="mb-3">
                    <label for="price" class="form-label">Product Price</label>
                    <input type="text" class="form-control" id="price" name="price"
                        aria-describedby="price">
                </div>

                <div class="mb-3">
                    <label for="description" class="form-label">Product Description</label>
                    <input type="text" class="form-control" id="description" name="description"
                        aria-describedby="description">
                </div>

                <div class="mb-3">
                    <label for="description" class="form-label">Product Description</label><br>
                    <select class="form-select" aria-label="Default select example" name="category_id">
                        <option value="">----</option>
                        @foreach ($category as $item)
                            
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
        
                      </select>
                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
