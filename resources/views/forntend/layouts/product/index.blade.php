@extends('forntend/layouts/master')

@section('title')
Product Index
@endsection


@section('Content')
<div class = "container my-4">
<div class = "card">
<div class = "card-header">
<a   href  = "{{ route ('create.product') }}"><button style = "float: right" class = "btn btn-success">Add
    Product</button></a>
            <h2>All Product</h2>
        </div>
        <div   class = "card-body">
        <table class = "table">
                <thead>
                    <tr>
                        <th scope = "col">Id</th>
                        <th scope = "col">Name</th>
                        <th scope = "col">Title</th>
                        <th scope = "col">Price</th>
                        <th scope = "col">Description</th>
                        <th scope = "col">Category</th>
                        <th scope = "col" class="text-center">Actions</th>
                    </tr>
                </thead>



                <tbody>

                    @foreach ($data as $item)
                  
                    <tr> 
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->price}}</td>
                        <td>{{$item->description}}</td>
                        <td>{{$item->category->name}}</td>
                        <td class = 'mx-2 d-flex justify-content-center'>
                        <a  href  = "{{ route ('view.product',$item->id) }}"><button
                            class = "btn btn-success mr-1">View</button></a>

                        <a  href  = "{{ route ('edit.product',$item->id) }}"><button
                            class = "btn btn-primary mr-1">Edit</button></a>

                        <a  href  = "{{ route ('destory.product',$item->id) }}"><button
                            class = "btn btn-danger">Delete</button></a>
                        </td>
                    </tr> 

                    @endforeach
                </tbody>
            </table>
        </div>
       
    </div>
</div>


@endsection
